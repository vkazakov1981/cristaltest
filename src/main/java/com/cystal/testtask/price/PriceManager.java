package com.cystal.testtask.price;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class PriceManager {

	/**
	 * Key - as Pair of product id and price id
	 * Value - List of prices
	 */
	private final Map<PriceKey, List<Price>> pricesMap = new HashMap<PriceKey, List<Price>>();
	
	public List<Price> mergePrices(List<Price> originalPricesList, List<Price> newPricesList) {
		if (originalPricesList != null) {
			for (Price price : originalPricesList) {
				addNewPrice(price);
			}
		}

		if (newPricesList != null) {
			for (Price price : newPricesList) {
				addNewPrice(price);
			}
		}

		return colletAllPricesToAnList();
	}

	private void addNewPrice(Price newPrice) {
		PriceKey key = new PriceKey(newPrice.getProductCode(), newPrice.getPriceNumber());
		List<Price> priceListOfProduct = pricesMap.get(key);
		if (priceListOfProduct == null) {
			// no prices for the product with this key
			List<Price> priceOfList = new ArrayList<Price>();
			priceOfList.add(newPrice);
			pricesMap.put(key, priceOfList);
		} else {
			// the price(s) is existing for this product
			// need to merge
			List<Price> newPricesForProduct = new ArrayList<Price>();
			boolean putNewPrice = true;
			for (Price oldPrice : priceListOfProduct) {
				putOlderstPrice(newPricesForProduct, newPrice, oldPrice, putNewPrice);
				putNewPrice = false;
			}
			pricesMap.put(key, newPricesForProduct);
		}
	}

	private void putOlderstPrice(List<Price> newPrices, Price newPrice, Price oldPrice, boolean putNewPrice) {

		if (newPrice.getValue() == oldPrice.getValue()) {
			Price.isDatesInDates(oldPrice, newPrice);
			Price mergedPrice = newPrice.clone();
			mergedPrice.setBegin(Long.min(newPrice.getBegin(), oldPrice.getBegin()));
			mergedPrice.setEnd(Long.max(newPrice.getEnd(), oldPrice.getEnd()));
			newPrices.add(mergedPrice);
			return;
		}
		SplitedPrices splitedOldPrices = Price.split(oldPrice, newPrice);
		if (splitedOldPrices.getBefore() != null) {
			boolean mergedPushed = false;
			if ((splitedOldPrices.getBefore().getValue() == newPrice.getValue()) &&
				splitedOldPrices.getBefore().getEnd() == newPrice.getBegin()
						|| newPrice.getEnd() == splitedOldPrices.getBefore().getEnd()) {
					Price mergedPrice = newPrice.clone();
					mergedPrice.setBegin(Long.min(newPrice.getBegin(), splitedOldPrices.getBefore().getBegin()));
					mergedPrice.setEnd(Long.max(newPrice.getEnd(), splitedOldPrices.getBefore().getEnd()));
					newPrices.add(mergedPrice);
					mergedPushed = true;
			}
			if (!mergedPushed) {
				newPrices.add(splitedOldPrices.getBefore());
			}
		}
		if (splitedOldPrices.getAfter() != null) {
			boolean mergedPushed = false;
			if ((splitedOldPrices.getAfter().getValue() == newPrice.getValue()) &&
				 (splitedOldPrices.getAfter().getEnd() == newPrice.getBegin()
						|| newPrice.getEnd() == splitedOldPrices.getAfter().getEnd())) {
					Price mergedPrice = newPrice.clone();
					mergedPrice.setBegin(Long.min(newPrice.getBegin(), splitedOldPrices.getAfter().getBegin()));
					mergedPrice.setEnd(Long.max(newPrice.getEnd(), splitedOldPrices.getAfter().getEnd()));
					newPrices.add(mergedPrice);
					mergedPushed = true;
			}
			if (!mergedPushed) {
				newPrices.add(splitedOldPrices.getAfter());
			}
		}
		if (putNewPrice) {
			newPrices.add(newPrice);
		}
	}

	private List<Price> colletAllPricesToAnList() {
		List<Price> resultList = new ArrayList<Price>();
		for (Entry<PriceKey, List<Price>> entry : pricesMap.entrySet()) {
			resultList.addAll(entry.getValue());
		}
		return resultList;
	}

	@Override
	public String toString() {
		return "PriceManager [pricesMap=" + pricesMap + "]";
	}
}
