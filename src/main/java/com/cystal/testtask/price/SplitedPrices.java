package com.cystal.testtask.price;

public class SplitedPrices {

	private final Price before;
	private final Price after;

	public SplitedPrices(Price before, Price after) {
		this.before = before;
		this.after = after;
	}

	public Price getBefore() {
		return before;
	}

	public Price getAfter() {
		return after;
	}

	public boolean hasSplited() {
		return before != null || after != null;
	}

	@Override
	public String toString() {
		return "SplitedPrices [before=" + before + ", after=" + after + "]";
	}
}
