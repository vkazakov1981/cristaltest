package com.cystal.testtask.price;

import java.util.Date;

public class Price implements Comparable<Price> {

	private final String productCode;
	private final int priceNumber;
	private final int depart;
	private long begin;
	private long end;
	private final int value;
	
	public Price(String productCode, int priceNumber, int depart, long begin, long end, int value) {
		this.productCode = productCode;
		this.priceNumber = priceNumber;
		this.depart = depart;
		this.begin = begin;
		this.end = end;
		this.value = value;
	}

	public String getProductCode() {
		return productCode;
	}

	public int getPriceNumber() {
		return priceNumber;
	}

	public int getDepart() {
		return depart;
	}

	public long getBegin() {
		return begin;
	}

	public void setBegin(long beginDate) {
		this.begin = beginDate;
	}
	
	public long getEnd() {
		return end;
	}
	
	public void setEnd(long endDate) {
		this.end = endDate;
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		
		Date beginDate = new Date(begin);
		Date endDate = new Date(end);
		
		return "Price [productCode=" + productCode + ", priceNumber=" + priceNumber
				+ ", depart=" + depart + ", begin=" + beginDate.toString() + ", end=" + endDate.toString() + ", value=" + value + "]\n";
	}

	public int compareTo(Price price) {
		return Integer.compare(this.getPriceNumber(), price.getPriceNumber());		
	}
    
	/**
	 * This in dates other
	 */
	public static boolean isDatesInDates(Price price1, Price price2) {
		if (price1.getBegin() >= price2.getBegin() && price1.getEnd() <= price2.getEnd()) {
			return true;
		}
		return false;		
	}
	
	/**
	 * Splitting price2 by price1.
	 * 
	 * Example 1:
	 *        dates of |----------------------| - price1
	 *        dates of     |------|             - price2
	 * result dates of |---|      |-----------| - splited early / later
	 * 	 
     * Example 2:
	 *        dates of |------|                 - price1
	 *        dates of     |------|             - price2
	 * result dates of |---|                    - splited early/null
	 * 
	 * Example 3:
	 *        dates of          |------------|  - price1
	 *        dates of     |------|             - price2
	 * result dates of            |----------|  - splited null/later
	 * 
	 * Example 4:
	 *        dates of       |--|               - price1
	 *        dates of     |------|             - price2
	 * result dates of                          - splited null/null
	 * @param price1
	 * @param price2
	 * @return
	 */
	
	public static SplitedPrices split(Price price1, Price price2) {
		Price early = null;
		
		if(price1.getBegin() < price2.getBegin()) {
			early = price1.clone();			
			early.setEnd(Long.min(price2.getBegin(), price1.getEnd())/* - 1000*/);
		}
		
		Price after = null;
		if(price1.getEnd() > price2.getEnd()) {
			after = price1.clone();
			Long.max(price2.getEnd(), price1.getBegin());
			after.setBegin(Long.max(price2.getEnd(), price1.getBegin()));
		}

		return new SplitedPrices(early, after);
	}
	
	@Override
	public Price clone() {
		return new Price(this.getProductCode(), this.getPriceNumber(), this.getDepart(),
				this.getBegin(), this.getEnd(), this.getValue());		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (begin ^ (begin >>> 32));
		result = prime * result + depart;
		result = prime * result + (int) (end ^ (end >>> 32));
		result = prime * result + priceNumber;
		result = prime * result + ((productCode == null) ? 0 : productCode.hashCode());
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Price other = (Price) obj;
		if (begin != other.begin)
			return false;
		if (depart != other.depart)
			return false;
		if (end != other.end)
			return false;
		if (priceNumber != other.priceNumber)
			return false;
		if (productCode == null) {
			if (other.productCode != null)
				return false;
		} else if (!productCode.equals(other.productCode))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
