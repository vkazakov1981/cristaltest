package com.cystal.testtask.price;

public class PriceKey {
	private final String productId;
	private final int priceNumber;

	public PriceKey(String productId, int priceNumber) {
		this.productId = productId;
		this.priceNumber = priceNumber;
	}

	public String getProductId() {
		return productId;
	}

	public int getPriceNumber() {
		return priceNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + priceNumber;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PriceKey other = (PriceKey) obj;
		if (priceNumber != other.priceNumber)
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}
}
