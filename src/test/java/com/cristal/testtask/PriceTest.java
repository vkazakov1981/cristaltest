package com.cristal.testtask;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;

import com.cystal.testtask.price.Price;

public class PriceTest {
	
    //  original prices
    private final String PRODUCT_ID = "122856";
    private final int PRICE_NUM = 1;
    private final int DEP_NUM = 1;
    private final int VALUE = 1;
	private final Date begin = new GregorianCalendar(2013, Calendar.JANUARY, 01, 0, 0, 0).getTime();	
	private final Date end = new GregorianCalendar(2013, Calendar.JANUARY, 31, 23, 59, 59).getTime();

    @Test
    public void testPriceConstructorAndGetters () {
    	Price price = new Price(PRODUCT_ID, PRICE_NUM, DEP_NUM , begin.getTime(), end.getTime(), VALUE);
    	
    	assertEquals(price.getProductCode(), PRODUCT_ID);
    	assertEquals(price.getPriceNumber(), PRICE_NUM);
    	assertEquals(price.getDepart(), DEP_NUM);
    	assertEquals(new Date(price.getBegin()), begin);
    	assertEquals(new Date(price.getEnd()), end);
    	assertEquals(price.getValue(), VALUE);
    	
    }
}
