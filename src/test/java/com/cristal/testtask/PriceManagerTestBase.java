package com.cristal.testtask;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public abstract class PriceManagerTestBase {
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    
    public long convEx(String in) throws ParseException{
        return sdf.parse(in).getTime();
    }
    
	public long conv(String in) {
		long result = 0;
		try {
			result = convEx(in);
		} catch (ParseException parseException) {
			throw new RuntimeException(parseException);
		}
		return result;
    }   
}
