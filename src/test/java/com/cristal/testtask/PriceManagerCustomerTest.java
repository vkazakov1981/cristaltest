package com.cristal.testtask;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.cystal.testtask.price.Price;
import com.cystal.testtask.price.PriceManager;

public class PriceManagerCustomerTest extends PriceManagerTestBase {
    //  original prices
	//122856 1 1 01.01.2013 00:00:00 31.01.2013 23:59:59 11000
	private final Price price1 = new Price("122856", 1, 1, conv("01.01.2013 00:00:00"), conv("31.01.2013 23:59:59"), 11000);
	
	//122856 2 1 10.01.2013 00:00:00 20.01.2013 23:59:59 99000
	private final Price price2 = new Price("122856", 2, 1, conv("10.01.2013 00:00:00"), conv("20.01.2013 23:59:59"), 99000);

	//6654 1 2 01.01.2013 00:00:00 31.01.2013 00:00:00 5000
	private final Price price3 = new Price("6654", 1, 2, conv("01.01.2013 00:00:00"), conv("31.01.2013 00:00:00"), 5000);
	
	/*
	 * new prices
	 */
	//122856 1 1 20.01.2013 00:00:00 20.02.2013 23:59:59 11000
	private final Price price4 = new Price("122856", 1, 1, conv("20.01.2013 00:00:00"), conv("20.02.2013 23:59:59"), 11000);
	
	//122856 2 1 15.01.2013 00:00:00 25.01.2013 23:59:59 92000	
	private final Price price5 = new Price("122856", 2, 1, conv("15.01.2013 00:00:00"), conv("25.01.2013 23:59:59"), 92000);
	
	//6654 1 2 12.01.2013 00:00:00 13.01.2013 00:00:00 4000
	private final Price price6 = new Price("6654", 1, 2, conv("12.01.2013 00:00:00"), conv("13.01.2013 00:00:00"), 4000);
	
	/* expected values
	122856 1 1 01.01.2013 00:00:00 20.02.2013 23:59:59 11000
	122856 2 1 10.01.2013 00:00:00 15.01.2013 00:00:00 99000
	122856 2 1 15.01.2013 00:00:00 25.01.2013 23:59:59 92000
	6654 1 2 01.01.2013 00:00:00 12.01.2013 00:00:00 5000
	6654 1 2 12.01.2013 00:00:00 13.01.2013 00:00:00 4000
	6654 1 2 13.01.2013 00:00:00 31.01.2013 00:00:00 5000
	*/
	//122856 1 1 01.01.2013 00:00:00 20.02.2013 23:59:59 11000
	private final Price priceExpected = new Price("122856", 1, 1, conv("01.01.2013 00:00:00"), conv("20.02.2013 23:59:59"), 11000);
	
	//122856 2 1 10.01.2013 00:00:00 15.01.2013 00:00:00 99000
	private final Price priceExpected2 = new Price("122856", 2, 1, conv("10.01.2013 00:00:00"), conv("15.01.2013 00:00:00"), 99000);
	
	//122856 2 1 15.01.2013 00:00:00 25.01.2013 23:59:59 92000
	private final Price priceExpected3 = new Price("122856", 2, 1, conv("15.01.2013 00:00:00"), conv("25.01.2013 23:59:59"), 92000);
	
	//6654 1 2 01.01.2013 00:00:00 12.01.2013 00:00:00 5000
	private final Price priceExpected4 = new Price("6654", 1, 2, conv("01.01.2013 00:00:00"), conv("12.01.2013 00:00:00"), 5000);
	
	//6654 1 2 12.01.2013 00:00:00 13.01.2013 00:00:00 4000
	private final Price priceExpected5 = new Price("6654", 1, 2, conv("12.01.2013 00:00:00"), conv("13.01.2013 00:00:00"), 4000);
	
	//6654 1 2 13.01.2013 00:00:00 31.01.2013 00:00:00 5000
	private final Price priceExpected6 = new Price("6654", 1, 2, conv("13.01.2013 00:00:00"), conv("31.01.2013 00:00:00"), 5000);
	
	private Set<Price> expectedList;
	
	@Before
	public void init() {
		expectedList = new HashSet<Price>();
		expectedList.add(priceExpected);
		expectedList.add(priceExpected2);
		expectedList.add(priceExpected3);
		expectedList.add(priceExpected4);
		expectedList.add(priceExpected5);
		expectedList.add(priceExpected6);
	}
	@Test
	public void mainTest() {
		PriceManager priceManager = new PriceManager();
		
		List<Price> priceOriginalList = new ArrayList<Price>();
		
		priceOriginalList.add(price1);
		priceOriginalList.add(price2);
		priceOriginalList.add(price3);
		
		List<Price> priceNewList = new ArrayList<Price>();
		
		priceNewList.add(price4);
		priceNewList.add(price5);
		priceNewList.add(price6);
		
		
		List<Price> mergedPrices = priceManager.mergePrices(priceOriginalList, priceNewList);
		
		for(Price price: mergedPrices) {
			expectedList.remove(price);
		}
		
		assertEquals("Expected list still has records", expectedList.size(), 0 );		
	}
}
