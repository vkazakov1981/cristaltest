package com.cristal.testtask;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.cystal.testtask.price.Price;
import com.cystal.testtask.price.PriceManager;

public class PriceManagerCrystalTest extends PriceManagerTestBase {
 
    @Test
    public void testMergePrices() throws ParseException {
        List<Price> oldPrices = new ArrayList<Price>();

        oldPrices.add(new Price("12345", 1, 1, conv("01.01.2013 00:00:00"), conv("05.01.2013 23:59:59"), 80));
        oldPrices.add(new Price("12345", 1, 1, conv("06.01.2013 00:00:00"), conv("10.01.2013 23:59:59"), 82));
        oldPrices.add(new Price("12345", 1, 1, conv("11.01.2013 00:00:00"), conv("15.01.2013 23:59:59"), 84));
        oldPrices.add(new Price("12345", 1, 1, conv("16.01.2013 00:00:00"), conv("20.01.2013 23:59:59"), 86));
        oldPrices.add(new Price("12345", 1, 1, conv("21.01.2013 00:00:00"), conv("25.01.2013 23:59:59"), 88));

        List<Price> newPrices = new ArrayList<Price>();

        newPrices.add(new Price("12345", 1, 1, conv("04.01.2013 00:00:00"), conv("07.01.2013 23:59:59"), 81));
        newPrices.add(new Price("12345", 1, 1, conv("09.01.2013 23:59:59"), conv("12.01.2013 23:59:59"), 83));
        newPrices.add(new Price("12345", 1, 1, conv("14.01.2013 00:00:00"), conv("17.01.2013 23:59:59"), 85));
        newPrices.add(new Price("12345", 1, 1, conv("19.01.2013 23:59:59"), conv("22.01.2013 23:59:59"), 87));

        PriceManager pm = new PriceManager();
        List<Price> unionPrices = pm.mergePrices(oldPrices, newPrices);

        assertEquals(9, unionPrices.size());

        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("01.01.2013 00:00:00"), conv("04.01.2013 00:00:00"), 80)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("04.01.2013 00:00:00"), conv("07.01.2013 23:59:59"), 81)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("07.01.2013 23:59:59"), conv("09.01.2013 23:59:59"), 82)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("09.01.2013 23:59:59"), conv("12.01.2013 23:59:59"), 83)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("12.01.2013 23:59:59"), conv("14.01.2013 00:00:00"), 84)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("14.01.2013 00:00:00"), conv("17.01.2013 23:59:59"), 85)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("17.01.2013 23:59:59"), conv("19.01.2013 23:59:59"), 86)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("19.01.2013 23:59:59"), conv("22.01.2013 23:59:59"), 87)));
        assertTrue(unionPrices.contains(new Price("12345", 1, 1, conv("22.01.2013 23:59:59"), conv("25.01.2013 23:59:59"), 88)));
    }
}
