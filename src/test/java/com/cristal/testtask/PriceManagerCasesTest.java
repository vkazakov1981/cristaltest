package com.cristal.testtask;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.cystal.testtask.price.Price;
import com.cystal.testtask.price.PriceManager;

public class PriceManagerCasesTest extends PriceManagerTestBase {
	
	private final Price priceExpectedIndepend1 = new Price("12", 1, 1, 
			conv("01.01.2013 00:00:00"),
			conv("01.01.2013 23:59:59"), 1);
	private final Price priceExpectedIndepend2 = new Price("12", 1, 1, 
			conv("01.01.2013 00:00:00"),
			conv("01.01.2013 23:59:59"), 1);

	private final Price priceOverlap1 = new Price("44", 1, 1, 
			conv("01.01.2013 00:00:00"), 
			conv("10.01.2013 00:00:00"),
			1);
	
	private final Price priceOverlap2 = new Price("44", 1, 1, 
			conv("05.01.2013 00:00:00"), 
			conv("13.01.2013 00:00:00"),
			1);

	private final Price priceExpectedOverlap = new Price("44", 1, 1, 
			conv("01.01.2013 00:00:00"),
			conv("13.01.2013 00:00:00"), 1);
	private final Price priceOverlapTest21 = new Price("44", 1, 1, 
			conv("05.01.2013 00:00:00"),
			conv("10.01.2013 00:00:00"), 1);

	private final Price priceOverlapTest22 = new Price("44", 1, 1, 
			conv("01.01.2013 00:00:00"),
			conv("15.01.2013 00:00:00"), 12);

	private final Price priceExpectedOverlapTest21 = new Price("44", 1, 1, 
			conv("01.01.2013 00:00:00"),
			conv("05.01.2013 00:00:00"), 12);

	private final Price priceExpectedOverlapTest22 = new Price("44", 1, 1, 
			conv("05.01.2013 00:00:00"),
			conv("10.01.2013 00:00:00"), 1);

	private final Price priceExpectedOverlapTest23 = new Price("44", 1, 1, 
			conv("10.01.2013 00:00:00"),
			conv("15.01.2013 00:00:00"), 12);

	private Set<Price> exptectedSet = null;
	
	public void initExpectedIndependValue() {
		exptectedSet = new HashSet<Price>();
		exptectedSet.add(priceExpectedIndepend1);
		exptectedSet.add(priceExpectedIndepend2);		
	}
	
	public void initExpectedOverlapValue() {
		exptectedSet = new HashSet<Price>();
		exptectedSet.add(priceExpectedOverlapTest21);				
		exptectedSet.add(priceExpectedOverlapTest22);
		exptectedSet.add(priceExpectedOverlapTest23);
	}
	
	@Test
	public void testTwoPricesIdependPriceNumber() {

		initExpectedIndependValue();
		List<Price> pricesList = new ArrayList<Price>(2);
		pricesList.add(priceExpectedIndepend1);
		pricesList.add(priceExpectedIndepend2);

		PriceManager priceManager = new PriceManager();
		List<Price> mergedList = priceManager.mergePrices(pricesList, null);
		for (Price price : mergedList) {
			exptectedSet.remove(price);
		}

		assertEquals("The result is wrong, the expected size should be 0", exptectedSet.size(), 0);
	}
	
	@Test
	public void testTwoPricesOverlapPrices() {

		List<Price> pricesList = new ArrayList<Price>(2);
		pricesList.add(priceOverlap2);
		pricesList.add(priceOverlap1);		

		PriceManager priceManager = new PriceManager();
		List<Price> mergedList = priceManager.mergePrices(pricesList, null);
		assertEquals("The result is wrong, the expected size should be 1", mergedList.size(), 1);
		
		Price mergedPrice = mergedList.get(0);		

		assertEquals("The merged price should be equals to expected price", priceExpectedOverlap, mergedPrice);
	}
	
	@Test
	public void testTwoPricesOverlapAndDiffValuesPrices() {
		initExpectedOverlapValue();
		List<Price> originalPricesList = new ArrayList<Price>(1);
		originalPricesList.add(priceOverlapTest22);
		
		List<Price> newPricesList = new ArrayList<Price>(1);
		newPricesList.add(priceOverlapTest21);
		

		PriceManager priceManager = new PriceManager();
		List<Price> mergedList = priceManager.mergePrices(originalPricesList, newPricesList);
		assertEquals("The result is wrong, the expected size should be 3", mergedList.size(), 3);
		
		for (Price price : mergedList) {
			exptectedSet.remove(price);
		}

		assertEquals("The result is wrong, the expected size should be 0", exptectedSet.size(), 0);
	}
}
